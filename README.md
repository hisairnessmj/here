<p align="center">
    <h3 align="center">hereProject </h3>
      <p align="center">
        Automation testing for geocoding service
      </p>
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#built-with">Built With</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation-and-running-all-tests">Installation and running all tests</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a>
       <ul>
        <li><a href="#running-tests-one-by-one">Running tests one by one</a></li>
        <li><a href="#running-tests-from-testng-file">Running tests from testng file</a></li>
      </ul>
    </li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->

### Built With

* [restAssured]()
* [testNG]()
* [Hamcrest]()
* [Jackson-databind]()
* [everit-jsonschema]()
* [apache_poi]()

<!-- GETTING STARTED -->

## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

* mvn
* java 1.8

### Installation and Running all tests

1. Clone the repo
   ```sh
   git clone https://gitlab.com/hisairnessmj/hereproject.git
   ```

2. Open it as a maven project in your IDE


3. Run command in project directory
   ```sh
   mvn clean test
   ```

<!-- USAGE EXAMPLES -->

## Usage

You can run project with `mvn clean test` command, it should build project and run tests inside. All tests are listed in
the file `testng.xml`, which is located in root directory. This file is pointed for the maven surefire plugin as suite
Xml file.

During tests all responses will be in console and report would be generated.

After that, you will have two reports native TestNG and extentreports. Notice that reports will be created only if you
run `testng.xml` or through console. Please open this report manually from folder.

- native TestNG report can be found in:

  ```sh
  target/surefire-reports/index.html
  ```

- extentreports report can be found in folder which is created during runtime:

  ```sh
  reports/myReport.html
  ```

All tests can be found in package:

  ```sh
  src/test/java/geocoding/get
  ```

In the folder `src/test/resources/dd` excel template is located. The purpose of it is to put data for data-driven
testing for one of tests. All data will be read and sent from the file.

In the folder `src/test/resources/schemas` here is json schema to validate responses in json with. Based on draft-04
version. Response will be validated and violations showed in console.

In the folder `src/test/resources` here is credentials.properties file with API key.

### Running tests one by one

Every test can be run in IDE from its class as TestNG test. Thus, one by one run provide more details.

### Running tests from testng file

All tests can be run from `testng.xml` file as well.