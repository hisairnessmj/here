package geocoding.get;

import io.restassured.response.Response;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.testng.annotations.Test;
import utils.PropertiesReader;

import static io.restassured.RestAssured.given;

class GetCoordinatesCheckJson {
    String apiKey = PropertiesReader.getData("apiKey");

    @Test
    public void getCoordinatesCheckJson() {
        Response response = given()
                .when()
                .get(String.format("https://geocode.search.hereapi.com/v1/geocode" +
                                "?q=%s&at=52.68113404624225,14.050807492171153" +
                                "&show=parsing,tz,countryInfo,streetInfo&politicalView=MAR&apiKey=%s",
                        "Deulstra%C3%9Fe+30%2C+Berlin", apiKey))
                .then()
                .statusCode(200)
                .statusLine("HTTP/1.1 200 OK")
                .header("Content-Type", "application/json")
                .and()
                .extract().response();

        JSONObject jsonSubject = new JSONObject(
                new JSONTokener(response.getBody().asString()));
        JSONObject jsonSchema = new JSONObject(
                new JSONTokener(getClass().getClassLoader().getResourceAsStream("schemas/GetCoordinates.json")));

        Schema schema = SchemaLoader.load(jsonSchema);
        try {
            schema.validate(jsonSubject);
        } catch (ValidationException e) {
            System.out.println(e.getMessage());
            e.getCausingExceptions().stream()
                    .map(ValidationException::getMessage)
                    .forEach(System.out::println);
        }
    }
}
