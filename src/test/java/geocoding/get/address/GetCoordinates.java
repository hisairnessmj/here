package geocoding.get.address;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import utils.Patterns;
import utils.PropertiesReader;

import java.util.regex.Pattern;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class GetCoordinates {
    String apiKey = PropertiesReader.getData("apiKey");

    @Test
    public void getCoordinates() {
        Response response = given()
                .when()
                    .get(String.format("https://geocode.search.hereapi.com/v1/geocode" +
                            "?q=%s&apiKey=%s", "Deulstraße+30%2C+Berlin", apiKey))
                .then()
                    .statusCode(200)
                    .statusLine("HTTP/1.1 200 OK")
                    .log().body()
                    .header("Content-Type", "application/json")
                    .header("X-Request-Id", matchesPattern(Patterns.REQUEST_ID.getPattern()))
                    .assertThat().body("items.resultType[0]", equalTo("houseNumber"))
                .and()
                    .assertThat().body("items.address.district[0]", equalTo("Oberschöneweide"))
                .and()
                    .assertThat().body("items.address.street[0]", equalTo("Deulstraße"))
                .and()
                    .extract().response();

        JsonPath jsonpath = response.jsonPath();
        Pattern patLat = Pattern.compile("(\\[52.4646\\d+\\])");
        Pattern patLng = Pattern.compile("(\\[13.5117\\d+\\])");

        assertThat(jsonpath.get("items.position.lat").toString(), matchesPattern(patLat));
        assertThat(jsonpath.get("items.position.lng").toString(), matchesPattern(patLng));
    }
}
