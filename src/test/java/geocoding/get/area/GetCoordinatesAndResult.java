package geocoding.get.area;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import utils.Patterns;
import utils.PropertiesReader;

import java.util.regex.Pattern;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class GetCoordinatesAndResult {
    String apiKey = PropertiesReader.getData("apiKey");

    @Test
    public void getCoordinatesAndResult() {
        Response response = given()
                .when()
                    .get(String.format("https://geocode.search.hereapi.com/v1/geocode" +
                            "?q=%s&apiKey=%s", "Тольятти", apiKey))
                .then()
                    .statusCode(200)
                    .statusLine("HTTP/1.1 200 OK")
                    .log().body()
                    .header("Content-Type", "application/json")
                    .header("X-Request-Id", matchesPattern(Patterns.REQUEST_ID.getPattern()))
                    .assertThat().body("items.resultType[0]", equalTo("locality"))
                .and()
                    .assertThat().body("items.localityType[0]", equalTo("city"))
                .and()
                    .assertThat().body("items.address.countryCode[0]", equalTo("RUS"))
                .and()
                    .assertThat().body("items.address.state[0]", containsString("Приволжский"))
                .and()
                    .assertThat().body("items.address.postalCode[0]", equalTo("445021"))
                .and()
                    .extract().response();

        JsonPath jsonpath = response.jsonPath();
        Pattern patLat = Pattern.compile("(\\[53.5088\\d+\\])");
        Pattern patLng = Pattern.compile("(\\[49.4215\\d+\\])");

        assertThat(jsonpath.get("items.position.lat").toString(), matchesPattern(patLat));
        assertThat(jsonpath.get("items.position.lng").toString(), matchesPattern(patLng));
    }
}
