package geocoding.get.dataDriven;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utils.Patterns;
import utils.PropertiesReader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class GetCoordinatesDataRowsIn {
    String apiKey = PropertiesReader.getData("apiKey");

    @Test(dataProvider = "streetDataProvider")
    public void getCoordinatesDataRowsIn(String streetname, String in,
                               String resultLat, String resultLng,
                               String cityname) {
        Response response = given()
                .when()
                .get(String.format("https://geocode.search.hereapi.com/v1/geocode" +
                        "?q=%s&in=%s&apiKey=%s", streetname, in, apiKey))
                .then()
                .statusCode(200)
                .statusLine("HTTP/1.1 200 OK")
                .log().body()
                .header("Content-Type", "application/json")
                .header("X-Request-Id", matchesPattern(Patterns.REQUEST_ID.getPattern()))
                .and()
                .extract().response();

        JsonPath jsonpath = response.jsonPath();

        if (!resultLat.equals("-")) {
            assertThat(jsonpath.get("items[0].position.lat").toString(), equalTo(resultLat));
            assertThat(jsonpath.get("items[0].position.lng").toString(), equalTo(resultLng));
            assertThat(jsonpath.get("items[0].address.city").toString(), equalTo(cityname));
            assertThat(jsonpath.getList("items").size(), greaterThan(10));
        }
    }

    @DataProvider(name = "streetDataProvider")
    Object[][] streetDataProvider() throws IOException, InvalidFormatException {
        String path = System.getProperty("user.dir") + "\\src\\test\\resources\\dd\\geodata.xlsx";

        OPCPackage pkg = OPCPackage.open(new File(path));
        XSSFWorkbook wb = new XSSFWorkbook(pkg);
        XSSFSheet sheet = wb.getSheet("List2");
        int rownum = sheet.getLastRowNum();
        int colcount = sheet.getRow(0).getLastCellNum();

        String streetdata[][] = new String[rownum][colcount];
        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                streetdata[i - 1][j] = String.valueOf(sheet.getRow(i).getCell(j));
            }
        }
        return streetdata;
    }
}
