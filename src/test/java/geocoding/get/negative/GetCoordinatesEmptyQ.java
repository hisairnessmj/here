package geocoding.get.negative;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import utils.Patterns;
import utils.PropertiesReader;

import java.util.regex.Pattern;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.matchesPattern;

public class GetCoordinatesEmptyQ {
    String apiKey = PropertiesReader.getData("apiKey");

    @Test
    public void getCoordinatesEmptyQ() {
        Response response = given()
                .when()
                    .get(String.format("https://geocode.search.hereapi.com/v1/geocode" +
                            "?q=&apiKey=%s", apiKey))
                .then()
                    .statusCode(400)
                    .statusLine("HTTP/1.1 400 Bad Request")
                    .log().body()
                    .header("Content-Type", "application/json")
                    .assertThat().body("title", equalTo("Illegal input for parameter 'q'"))
                .and()
                    .assertThat().body("cause", equalTo("Actual parameter value: ''"))
                .and()
                    .assertThat().body("action", equalTo("Value must be non-empty"))
                .and()
                    .assertThat().body("requestId", matchesPattern(Patterns.REQUEST_ID.getPattern()))
                .and()
                    .extract().response();

        JsonPath jsonpath = response.jsonPath();
        Pattern patCorrelationId = Patterns.CORRELATION_ID.getPattern();

        assertThat(jsonpath.get("correlationId").toString(), matchesPattern(patCorrelationId));
    }
}
