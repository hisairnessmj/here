package geocoding.get.negative;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import utils.Patterns;
import utils.PropertiesReader;

import java.util.regex.Pattern;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.matchesPattern;

public class GetCoordinatesWrongCoordinates {
    String apiKey = PropertiesReader.getData("apiKey");

    @Test
    public void getCoordinatesWrongCoordinates() {
        Response response = given()
                .when()
                    .get(String.format("https://geocode.search.hereapi.com/v1/geocode" +
                            "?q=Starbucks&at=51.581500984858884, 7.446028039995068&apiKey=%s", apiKey))
                .then()
                    .statusCode(400)
                    .statusLine("HTTP/1.1 400 Bad Request")
                    .log().body()
                    .header("Content-Type", "application/json")
                    .assertThat().body("title", equalTo("Illegal input for parameter 'at'"))
                .and()
                    .assertThat().body("cause", equalTo("Actual parameter value: '51.581500984858884, 7.446028039995068'"))
                .and()
                    .assertThat().body("action", equalTo("Expecting string formatted like 43.42387,15.490238"))
                .and()
                    .assertThat().body("requestId", matchesPattern(Patterns.REQUEST_ID.getPattern()))
                .and()
                    .extract().response();

        JsonPath jsonpath = response.jsonPath();
        Pattern patCorrelationId = Patterns.CORRELATION_ID.getPattern();

        assertThat(jsonpath.get("correlationId").toString(), matchesPattern(patCorrelationId));
    }
}
