package geocoding.get.negative;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import utils.Patterns;
import utils.PropertiesReader;

import java.util.regex.Pattern;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.matchesPattern;

public class GetCoordinatesWrongLatitude {
    String apiKey = PropertiesReader.getData("apiKey");

    @Test
    public void getCoordinatesWrongLatitude() {
        Response response = given()
                .when()
                    .get(String.format("https://geocode.search.hereapi.com/v1/geocode" +
                            "?q=Starbucks&at=-100,-50&apiKey=%s", apiKey))
                .then()
                    .statusCode(400)
                    .statusLine("HTTP/1.1 400 Bad Request")
                    .log().body()
                    .header("Content-Type", "application/json")
                    .assertThat().body("title", equalTo("Illegal input for parameter 'at'"))
                .and()
                    .assertThat().body("cause", equalTo("Actual parameter value: '-100,-50'"))
                .and()
                    .assertThat().body("action", equalTo("requirement failed: Latitude must be between -90 <= X <= 90, actual: -100.0"))
                .and()
                    .assertThat().body("requestId", matchesPattern(Patterns.REQUEST_ID.getPattern()))
                .and()
                    .extract().response();

        JsonPath jsonpath = response.jsonPath();
        Pattern patCorrelationId = Patterns.CORRELATION_ID.getPattern();

        assertThat(jsonpath.get("correlationId").toString(), matchesPattern(patCorrelationId));
    }
}
