package geocoding.get.place;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import utils.Patterns;
import utils.PropertiesReader;

import java.util.regex.Pattern;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.matchesPattern;

public class GetCoordinatesAndPlace {
    String apiKey = PropertiesReader.getData("apiKey");

    @Test
    public void getCoordinates() {
        Response response = given()
                .when()
                .get(String.format("https://geocode.search.hereapi.com/v1/geocode" +
                        "?q=%s&apiKey=%s", "Kaufland,Schnellerstraße", apiKey))
                .then()
                .statusCode(200)
                .statusLine("HTTP/1.1 200 OK")
                .log().body()
                .header("Content-Type", "application/json")
                .header("X-Request-Id", matchesPattern(Patterns.REQUEST_ID.getPattern()))
                .assertThat().body("items.resultType[0]", equalTo("place"))
                .and()
                .assertThat().body("items.categories.id[0][0]", equalTo("600-6300-0066"))
                .and()
                .assertThat().body("items.categories.name[0][0]", equalTo("Supermarkt"))
                .and()
                .assertThat().body("items.foodTypes.id[0][0]", equalTo("302-000"))
                .and()
                .extract().response();

        JsonPath jsonpath = response.jsonPath();
        Pattern patLat = Pattern.compile("(\\[52.4564\\d+\\])");
        Pattern patLng = Pattern.compile("(\\[13.5092\\d+\\])");

        assertThat(jsonpath.get("items.position.lat").toString(), matchesPattern(patLat));
        assertThat(jsonpath.get("items.position.lng").toString(), matchesPattern(patLng));
    }
}
