package utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class Listener extends TestListenerAdapter {
    public ExtentHtmlReporter htmlReporter;
    public ExtentReports extent;
    public ExtentTest test;

    public void onStart(ITestContext context) {
        htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/reports/myReport.html");

        htmlReporter.config().setDocumentTitle("Report");
        htmlReporter.config().setReportName("Rest API Testing");
        htmlReporter.config().setTheme(Theme.DARK);

        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);
        extent.setSystemInfo("Host name", "local host");
        extent.setSystemInfo("Env", "QA");
    }

    public void onTestSuccess(ITestResult result) {
        test = extent.createTest(result.getName());

        test.log(Status.PASS, "Test case PASSED IS" + result.getName());
    }

    public void onTestFailure(ITestResult result) {
        test = extent.createTest(result.getName());

        test.log(Status.FAIL, "Test case FAILED IS" + result.getName());
        test.log(Status.FAIL, "Test case FAILED IS" + result.getThrowable());
    }

    public void onTestSkipped(ITestResult result) {
        test = extent.createTest(result.getName());

        test.log(Status.SKIP, "Test case SKIPPED IS" + result.getName());
    }

    public void onFinish(ITestContext context) {
        extent.flush();
    }
}
