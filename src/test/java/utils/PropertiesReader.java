package utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {
    private static final PropertiesReader instance = new PropertiesReader();
    public static Properties properties;

    private PropertiesReader() {
        loadData();
    }

    public void loadData() {
        try {
            FileInputStream Locator = new FileInputStream("src/test/resources/credentials.properties");
            properties = new Properties();
            properties.load(Locator);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getData(String ElementName) throws IllegalArgumentException {
        String data = properties.getProperty(ElementName);
        return data;
    }
}
